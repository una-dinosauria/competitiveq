function [ b, xr ] = encode( x, C, m, H )
%ENCODE Encode a vector using hierarchical beam search.

h = size( C{1}, 2 ); % Number of entries in each codebook.

% Get the first H candidates
xrs          = bsxfun(@minus, x, C{1} ); % Get all h residuals
qerrs        = sum( xrs.^2, 1 );         % Compute the qerrors
[~,sort_idx] = sort( qerrs );            % Sort and get the top H indices
sort_idx     = sort_idx(1:H);
xrs          = xrs(:, sort_idx);         % The top H residuals

% Structure for residuals at lth level
new_res   = cell(1, H);
new_qerrs = cell(1, H);
new_bs    = zeros( H*h, m, 'int16' );

% intiialize the code candidates
for i = 1:H
  new_bs( (i-1)*h+1 : i*h, 1 ) = sort_idx(i);
end

% Go through the rest of the codebooks
for i = 2:m

  % Compute new residuals and costs
  Ci = C{i};

  for j = 1:H
    new_res{j}   = bsxfun(@minus, xrs(:,j), Ci );
    new_qerrs{j} = sum( new_res{j}.^2, 1 );
    new_bs( (j-1)*h+1 : j*h, i ) = 1:h;
  end

  % Find the top H candidates
  all_qerrs = cell2mat( new_qerrs );
  [~, sort_idx] = sort( all_qerrs ); % Sort and get the top H indices
  sort_idx = sort_idx(1:H);  

  all_res = cell2mat( new_res );
  xrs     = all_res( :,  sort_idx);
  top_bs  = new_bs(sort_idx, 1:i );
  
  % Reset the candidate bs
  for j = 1:H
    for k = 1:h
      new_bs( (j-1)*h+k, 1:i ) = top_bs(j, :);
    end
  end

end

b  = new_bs(1,:);
xr = xrs(:,1);

end

