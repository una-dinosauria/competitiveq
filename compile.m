
% Compile utilities of OPQ by @norouzi.
cd utils/
mex euc_nn_mex.cc CXXFLAGS="\$CXXFLAGS -fopenmp -Wall" LDFLAGS="\$LDFLAGS -fopenmp";
mex kmeans_iter_mex.cc CXXFLAGS="\$CXXFLAGS -Wall"

% Get back to the top directory.
cd ../