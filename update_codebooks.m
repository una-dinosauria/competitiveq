function [ C ] = update_codebooks( C, m, xr, b, lrs )
%UPDATE_CODEBOOKS Update codebooks using gradient descent.
%
% X = SQ_decode( C, m, xr, b, lrs) returns the updated codebooks C.
% 
% Input
%   C         : m-long cell array. Each entry contains a 
%               d-by-nwords matrix of codewords used to encode the 
%               database.
%   m         : number of codebooks
%   xr        : residual vector (gradient direction)
%   b         : Vector of m integers. The encoding of x, or "winner"
%               codevectors. Used to decide which entries will be updated
%   lrs       : m-long vector. Learning rate for each codebook.
%
% Output
%   C         : m-long cell array. The updated codebooks.

% --
% Julieta

for i = 1:m
  bi = b(i);
  C{i}(:,bi) = C{i}(:,bi) + 2 * lrs(i) * xr;
end

end

