
Get the data by running the following commands:

```
mkdir data
cd data
wget ftp://ftp.irisa.fr/local/texmex/corpus/sift.tar.gz
tar -xvzf sift.tar.gz
rm sift.tar.gz
cd ..
```

Then, compile the cpp utilities running

```
compile.m
```

Finally, run

```
run_competitiveq.m
```

for a demo.