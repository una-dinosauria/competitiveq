
% Add utils
addpath utils

% Load the data
X = fvecs_read( 'data/sift/sift_learn.fvecs' );
X = X(:, 1:1e4);

k = 256; % Number of vectors in each codebook
m = 8;   % Number of codebooks
V = true;

% Initialize with RVQ
[B, C] = SQ_initialize_kmeans(X, k, m, 100, V);

lr = 0.5; % learning rate
H  = 32;  % depth for beam search-like algorithm
n_its = 250; % total number of training iterations.

[d, n] = size(X); % dimensionality and number of data point

%% Run competitiveQ

% Compute the learning rate for each layer (Eq. 26)
lrs = zeros(m, 1);
for i = 1:m
  lrs(i) = (1  ./ ( log2(i) +  1 )) * lr;
end
lrs = lrs ./ sum(lrs);
lrs = lrs .* lr;

for i = 1:n_its

  % Encode each vector
  for j = 1:n

    x  = X(:,j);

    [bj, xr] = encode(x, C, m, H);

    B(:,j) = bj; % Update the code

    code_after = bj;

    if mod(j, 100) == 0
      qerr = get_qerror(X, B, C);
      fprintf( 'Error after %d iterations / %d samples is %.2f\n', i, j, qerr );
    end

    % Update the codebooks
    C = update_codebooks(C, m, xr, bj, lrs);
  end

  % Compute overall quantization error
  qerr = get_qerror(X, B, C);
  fprintf( 'Error after %d iterations is %.2f\n', i, qerr );

  % Decrease the learning rate by 1%
  lr = 0.99 * lr;

  % Recompute the learning rates
  for i = 1:m
    lrs(i) = (1  ./ ( log2(i) +  1 )) * lr;
  end
  lrs = lrs ./ sum(lrs);
  lrs = lrs .* lr;
  
end